<?php

namespace Concat\Auth;

interface ClientStorageProvider
{
    //
    public function storeToken($token, $remember);

    //
    public function getToken();

    //
    public function hasToken();

    //
    public function deleteToken();
}

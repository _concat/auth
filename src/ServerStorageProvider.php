<?php

namespace Concat\Auth;

interface ServerStorageProvider
{
    // stores a hash for a given key
    public function storeHash($key, $hash);

    // stores a token for a given key
    public function storeToken($key, $token);

    // deletes the entry for the given key
    public function deleteKey($key);

    // destroys a token
    public function deleteToken($token);

        // returns a key for a given token
    public function getKey($token);

    // checks if the passphrase hash is correct
    public function getHash($key);

    public function storeResetToken($key, $token);

    public function getResetKey($token);

    public function deleteResetToken($token);

    // delete tokens considered dormant
    public function purge();
}

<?php

namespace Concat\Auth;

class Authenticator
{
    private $server;
    private $client;

    private $algorithm = PASSWORD_DEFAULT;

    public function __construct(ServerStorageProvider $server, ClientStorageProvider $client)
    {
        $this->server = $server;
        $this->client = $client;
    }

    // returns token
    public function authenticate($key, $remember = false)
    {
        // generate a token
        $token = $this->generateToken();

        // store the token as a valid session
        $this->server->storeToken($key, $token);

        // store the token on the client side
        $this->client->storeToken($token, $remember);

        // return the token
        return $token;
    }

    public function match($key, $pass)
    {
        $hash = $this->server->getHash($key);

        if ($hash && password_verify($pass, $hash)) {
            // may be worthwhile checking if the password requires a rehash
            if (password_needs_rehash($hash, $this->algorithm)) {
                // password doesn't follow the specified hash alg
                $this->register($key, $pass);
            }

            return true;
        }

        return false;
    }

    // returns key currently authenticated on client
    // someone could steal your cookie though sorry
    public function verify()
    {
        if ($this->client->hasToken()) {
            $token = $this->client->getToken();

            return $this->server->getKey($token);
        }
    }

    //
    public function revoke($token)
    {
        //
        $this->server->deleteToken($token);

        //
        $this->client->deleteToken();
    }

    public function generateResetToken($key)
    {
        //
        $token = $this->generateToken();

        //
        $this->server->storeResetToken($key, $token);

        return $token;
    }

    public function resetPassword($token, $pass)
    {
        //
        $key = $this->server->getResetKey($token);

        if ($key) {
            //
            $this->server->deleteResetToken($token);

            //
            $this->register($key, $pass);

            return $key;
        }

        // invalid reset token?
    }

    public function unregister($key)
    {
        //
        $this->server->deleteKey($key);
    }

    public function register($key, $pass)
    {
        // generate a hash first
        $hash = $this->generateHash($pass);

        // store the hash as a valid passphrase hash
        $this->server->storeHash($key, $hash);

        // return the hash
        return $hash;
    }

    public function setHashAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    private function generateHash($pass)
    {
        return password_hash($pass, $this->algorithm);
    }

    private function generateToken()
    {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
}

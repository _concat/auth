<?php

namespace Concat\Auth;

class CookieStorage implements ClientStorageProvider
{
    const COOKIE_NAME = '_at';

    private $ttl = 60 * 60 * 24 * 365; // default 1 year for remember me expiry

    public function storeToken($token, $remember)
    {
        $expire = $remember ? time() + $this->ttl : 0;
        setcookie(self::COOKIE_NAME, $token, $expire);
    }

    public function deleteToken()
    {
        unset($_COOKIE[self::COOKIE_NAME]);
    }

    public function getToken()
    {
        if ($this->hasToken()) {
            return $_COOKIE[self::COOKIE_NAME];
        }
    }

    public function hasToken()
    {
        return isset($_COOKIE[self::COOKIE_NAME]);
    }

    public function setRememberTime($seconds)
    {
        $this->ttl = $seconds;
    }
}

<?php

namespace Concat\Auth\Tests;

use Concat\Auth\ServerStorageProvider;

class TestServerStorageProvider implements ServerStorageProvider
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
        $data = [
            'tokens' => [],
            'hashes' => [],
            'resets' => [],
        ];
        $this->update($data);
    }

    private function get()
    {
        return json_decode(file_get_contents($this->name), true);
    }

    private function update($data)
    {
        file_put_contents($this->name, json_encode($data));
    }

    // stores a hash for a given key
    public function storeHash($key, $hash)
    {
        $data = $this->get();

        $hashes = $data['hashes'];

        $hashes = array_filter(array_keys($hashes), function ($hash) use ($hashes, $key) {
            return $hashes[$hash] !== $key;
        });

        $hashes[$hash] = $key;

        $data['hashes'] = $hashes;

        $this->update($data);
    }

    // destroys a token
    public function deleteToken($token)
    {
        $data = $this->get();
        unset($data['tokens'][$token]);
        $this->update($data);
    }

    // stores a token for a given key
    public function storeToken($key, $token)
    {
        $data = $this->get();
        $data['tokens'][$token] = $key;
        $this->update($data);
    }

    public function deleteKey($key)
    {
        $data = $this->get();
        extract($data);

        $tokens = array_filter(array_keys($tokens), function ($token) use ($tokens, $key) {
            return $tokens[$token] !== $key;
        });

        $hashes = array_filter(array_keys($hashes), function ($hash) use ($hashes, $key) {
            return $hashes[$hash] !== $key;
        });

        $resets = array_filter(array_keys($resets), function ($token) use ($resets, $key) {
            return $resets[$token] !== $key;
        });

        $this->update(compact('tokens', 'hashes', 'resets'));
    }

    // returns a key for a given token
    public function getKey($token)
    {
        $data = $this->get();

        return @$data['tokens'][$token];
    }

    public function getHash($key)
    {
        $data = $this->get();
        foreach ($data['hashes'] as $hash => $key) {
            if ($key === $key) {
                return $hash;
            }
        }
    }

    public function purge()
    {
    }

    public function storeResetToken($key, $token)
    {
        $data = $this->get();
        $data['resets'][$token] = $key;
        $this->update($data);
    }

    public function getResetKey($token)
    {
        $data = $this->get();

        return @$data['resets'][$token];
    }

    public function deleteResetToken($token)
    {
        $data = $this->get();
        unset($data['resets'][$token]);
        $this->update($data);
    }
}

<?php

namespace Concat\Auth\Tests;

use Concat\Auth\Authenticator;

class AuthTest extends \PHPUnit_Framework_TestCase
{
    private $auth;

    const FILE = "test.json";
    const K = "key";
    const P = "pass";

    private $client;
    private $server;

    public function tearDown()
    {
        unlink(self::FILE);
    }

    private function create()
    {
        $this->server = new TestServerStorageProvider(self::FILE);
        $this->client = new TestClientStorageProvider();

        return new Authenticator($this->server, $this->client);
    }

    public function setUp()
    {
        $this->auth = $this->create();

        $this->auth->register(self::K, self::P);
    }

    public function test_match()
    {
        $this->auth->register(self::K, self::P);

        $this->assertTrue($this->auth->match(self::K, self::P));
        $this->assertFalse($this->auth->match(self::K, 0));
    }

    public function test_auth()
    {
        $this->auth->register(self::K, self::P);

        $token = $this->auth->authenticate(self::K);

        $this->assertEquals(1, preg_match('/^[a-f0-9]{64}$/', $token));
    }

    public function test_verify()
    {
        $token = $this->auth->authenticate(self::K);

        $this->assertEquals(self::K, $this->auth->verify());

        $this->client->storeToken("", false);
        $this->assertNull($this->auth->verify());
    }

    public function test_delete()
    {
        $token = $this->auth->authenticate(self::K);

        $this->assertEquals(self::K, $this->auth->verify());

        $this->auth->revoke($token);

        $this->assertNull($this->auth->verify());
    }

    public function test_unregister()
    {
        $this->auth->register(self::K, self::P);
        $this->assertTrue($this->auth->match(self::K, self::P));

        $this->auth->unregister(self::K);
        $this->assertFalse($this->auth->match(self::K, self::P));
    }

    public function test_reset()
    {
        $this->auth->register(self::K, self::P);
        $this->assertTrue($this->auth->match(self::K, self::P));

        $token = $this->auth->generateResetToken(self::K);
        $this->assertEquals(1, preg_match('/^[a-f0-9]{64}$/', $token));

        $this->auth->resetPassword($token, 'new_pass');

        $this->assertFalse($this->auth->match(self::K, self::P));
        $this->assertTrue($this->auth->match(self::K, 'new_pass'));

        // also test invalid token
        $this->auth->resetPassword('invalid_token', 'new_pass');
        $this->assertTrue($this->auth->match(self::K, 'new_pass'));
    }
}

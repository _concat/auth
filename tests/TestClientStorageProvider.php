<?php

namespace Concat\Auth\Tests;

use Concat\Auth\ClientStorageProvider;

class TestClientStorageProvider implements ClientStorageProvider
{
    const COOKIE_NAME = 'a';

    private $ttl = 60 * 60 * 24 * 365; // default 1 year for remember me expiry

    private $store;

    public function storeToken($token, $remember)
    {
        $this->store = [$token, $remember];
    }

    public function deleteToken()
    {
        $this->store = null;
    }

    public function hasToken()
    {
        return $this->store !== null;
    }

    public function getToken()
    {
        return $this->store[0];
    }
}
